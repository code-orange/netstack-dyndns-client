#!/bin/bash

set -e

. /etc/code-orange/netstack-dyndns-client/dyndns.conf

/usr/bin/wget "http://api.srvfarm.net/publicapi/api/ddns/update?ddnsuser=${dyndns_user}&ddnstoken=${dyndns_token}&hostname=${dyndns_host}" -O /dev/null
